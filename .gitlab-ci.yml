# Only spawn workflows for MRs or protected branches
workflow:
  rules:
  - if: $CI_PIPELINE_SOURCE == "push" && $CI_COMMIT_REF_PROTECTED != "true"
    when: never
  - when: always


stages:
- setup
- validate
- test
- lint


variables:
  # Most jobs require the submodules, those that don't will override this
  GIT_SUBMODULE_STRATEGY: recursive

  # Add a transfer progress bar for artifacts
  TRANSFER_METER_FREQUENCY: 2s

  # Use fastzip to package caches and artifacts
  FF_USE_FASTZIP: 'true'
  ARTIFACT_COMPRESSION_LEVEL: default
  CACHE_COMPRESSION_LEVEL: fastest

  # Retry various preliminary job stages (network errors)
  GET_SOURCES_ATTEMPTS: 3
  ARTIFACTS_DOWNLOAD_ATTEMPTS: 3
  EXECUTOR_JOB_SECTION_ATTEMPTS: 3


default:
  # Most jobs can be interrupted and should be retried if failed for mysterious reasons
  interruptible: true
  retry:
    max: 2
    when:
    - unknown_failure
    - api_failure
    - stuck_or_timeout_failure
    - runner_system_failure


# Many jobs use ccache to speed up the build process. This job provides common settings for those.
.ccache job:
  cache:
  - key: ccache-$CI_JOB_NAME
    when: always
    paths:
    - .ccache/

  variables:
    CCACHE_DIR: '$CI_PROJECT_DIR/.ccache'
    CCACHE_BASEDIR: '$CI_PROJECT_DIR'
    CCACHE_NOCOMPRESS: 'true'
    CCACHE_MAXSIZE: '5G'
    CCACHE_MAXFILES: '0'

#----------------------------------------------
# Phase 0: Construct the predeps images
#----------------------------------------------

include:
# First build images that just contain the Spack-built dependencies. Since these are bound to
# a particular OS+ARCH, they can be shared between multiple final images, so we do this build
# first so that we don't end up rebuilding Spack packages a whole bunch of times.
- component: &ci_predeps gitlab.com/blue42u/ci.predeps@0.1.0
  inputs:
    name: spack-almalinux8-amd64
    context_dir: ci/predeps-spack/
    fallback_registry: &ci_predeps_fallback_registry registry.gitlab.com/hpctoolkit/hpctoolkit/ci.predeps
- component: *ci_predeps
  inputs:
    name: spack-ubuntu20.04-amd64
    context_dir: ci/predeps-spack/
    fallback_registry: *ci_predeps_fallback_registry
- component: *ci_predeps
  inputs:
    name: spack-ubuntu20.04-arm64
    context_dir: ci/predeps-spack/
    platform: linux/arm64
    job_tag: rice-linux-medium-arm64
    fallback_registry: *ci_predeps_fallback_registry
- component: *ci_predeps
  inputs:
    name: spack-fedora37-amd64
    context_dir: ci/predeps-spack/
    fallback_registry: *ci_predeps_fallback_registry

# Minimum variant, where all versions of Spack-built software are locked to their minimum allowed.
- component: *ci_predeps
  inputs:
    name: spack-min-almalinux8-amd64
    context_dir: ci/predeps-spack/
    fallback_registry: *ci_predeps_fallback_registry
- component: *ci_predeps
  inputs:
    name: spack-min-ubuntu20.04-amd64
    context_dir: ci/predeps-spack/
    fallback_registry: *ci_predeps_fallback_registry
- component: *ci_predeps
  inputs:
    name: spack-min-ubuntu20.04-arm64
    context_dir: ci/predeps-spack/
    platform: linux/arm64
    job_tag: rice-linux-medium-arm64
    fallback_registry: *ci_predeps_fallback_registry
- component: *ci_predeps
  inputs:
    name: spack-min-fedora37-amd64
    context_dir: ci/predeps-spack/
    fallback_registry: *ci_predeps_fallback_registry

# These images are based on classic distrobutions and contain no vendor software
- component: *ci_predeps
  inputs:
    name: almalinux8-amd64
    context_dir: ci/predeps/
    fallback_registry: *ci_predeps_fallback_registry
- component: *ci_predeps
  inputs:
    name: ubuntu20.04-amd64
    context_dir: ci/predeps/
    fallback_registry: *ci_predeps_fallback_registry
- component: *ci_predeps
  inputs:
    name: ubuntu20.04-arm64
    context_dir: ci/predeps/
    platform: linux/arm64
    job_tag: rice-linux-medium-arm64
    fallback_registry: *ci_predeps_fallback_registry
- component: *ci_predeps
  inputs:
    name: fedora38-amd64
    context_dir: ci/predeps/
    fallback_registry: *ci_predeps_fallback_registry

- component: *ci_predeps
  inputs:
    name: min-almalinux8-amd64
    context_dir: ci/predeps/
    fallback_registry: *ci_predeps_fallback_registry
- component: *ci_predeps
  inputs:
    name: min-ubuntu20.04-amd64
    context_dir: ci/predeps/
    fallback_registry: *ci_predeps_fallback_registry
- component: *ci_predeps
  inputs:
    name: min-ubuntu20.04-arm64
    context_dir: ci/predeps/
    platform: linux/arm64
    job_tag: rice-linux-medium-arm64
    fallback_registry: *ci_predeps_fallback_registry
- component: *ci_predeps
  inputs:
    name: min-fedora38-amd64
    context_dir: ci/predeps/
    fallback_registry: *ci_predeps_fallback_registry

# Images based on (containing) Nvidia's CUDA Toolkit
- component: *ci_predeps
  inputs:
    name: cuda11.6-amd64
    context_dir: ci/predeps/
    fallback_registry: *ci_predeps_fallback_registry
- component: *ci_predeps
  inputs:
    name: cuda11.8-amd64
    context_dir: ci/predeps/
    fallback_registry: *ci_predeps_fallback_registry
- component: *ci_predeps
  inputs:
    name: cuda12.0-amd64
    context_dir: ci/predeps/
    fallback_registry: *ci_predeps_fallback_registry

- component: *ci_predeps
  inputs:
    name: min-cuda11.8-amd64
    context_dir: ci/predeps/
    fallback_registry: *ci_predeps_fallback_registry
- component: *ci_predeps
  inputs:
    name: min-cuda12.0-amd64
    context_dir: ci/predeps/
    fallback_registry: *ci_predeps_fallback_registry

# Images based on (containing) AMD's ROCm
- component: *ci_predeps
  inputs:
    name: rocm5.1-amd64
    context_dir: ci/predeps/
    fallback_registry: *ci_predeps_fallback_registry
- component: *ci_predeps
  inputs:
    name: rocm5.2-amd64
    context_dir: ci/predeps/
    fallback_registry: *ci_predeps_fallback_registry
- component: *ci_predeps
  inputs:
    name: rocm5.3-amd64
    context_dir: ci/predeps/
    fallback_registry: *ci_predeps_fallback_registry
- component: *ci_predeps
  inputs:
    name: rocm5.4-amd64
    context_dir: ci/predeps/
    fallback_registry: *ci_predeps_fallback_registry
- component: *ci_predeps
  inputs:
    name: rocm5.5-amd64
    context_dir: ci/predeps/
    fallback_registry: *ci_predeps_fallback_registry
- component: *ci_predeps
  inputs:
    name: rocm5.6-amd64
    context_dir: ci/predeps/
    fallback_registry: *ci_predeps_fallback_registry

- component: *ci_predeps
  inputs:
    name: min-rocm5.1-amd64
    context_dir: ci/predeps/
    fallback_registry: *ci_predeps_fallback_registry
- component: *ci_predeps
  inputs:
    name: min-rocm5.6-amd64
    context_dir: ci/predeps/
    fallback_registry: *ci_predeps_fallback_registry

# Image(s) based on (containing) Intel's Level Zero, IGC, GTPin, etc.
- component: *ci_predeps
  inputs:
    name: intel-amd64
    context_dir: ci/predeps/
    fallback_registry: *ci_predeps_fallback_registry
- component: *ci_predeps
  inputs:
    name: min-intel-amd64
    context_dir: ci/predeps/
    fallback_registry: *ci_predeps_fallback_registry

'predeps: [spack-almalinux8-amd64]':
  variables:
    PREDEPS_BUILD_ARG_SPACKOS: almalinux8
'predeps: [spack-ubuntu20.04-amd64]':
  variables:
    PREDEPS_BUILD_ARG_SPACKOS: ubuntu-focal
'predeps: [spack-ubuntu20.04-arm64]':
  variables:
    PREDEPS_BUILD_ARG_SPACKOS: ubuntu-focal
'predeps: [spack-fedora37-amd64]':
  variables:
    PREDEPS_BUILD_ARG_SPACKOS: fedora37

'predeps: [spack-min-almalinux8-amd64]':
  variables:
    PREDEPS_BUILD_ARG_SPACKOS: almalinux8
    PREDEPS_BUILD_ARG_VERMODE: minimum
'predeps: [spack-min-ubuntu20.04-amd64]':
  variables:
    PREDEPS_BUILD_ARG_SPACKOS: ubuntu-focal
    PREDEPS_BUILD_ARG_VERMODE: minimum
'predeps: [spack-min-ubuntu20.04-arm64]':
  variables:
    PREDEPS_BUILD_ARG_SPACKOS: ubuntu-focal
    PREDEPS_BUILD_ARG_VERMODE: minimum
'predeps: [spack-min-fedora37-amd64]':
  variables:
    PREDEPS_BUILD_ARG_SPACKOS: fedora37
    PREDEPS_BUILD_ARG_VERMODE: minimum

'predeps: [almalinux8-amd64]':
  needs: ['predeps: [spack-almalinux8-amd64]']
  variables:
    PREDEPS_BUILD_ARG_BASE: quay.io/almalinux/almalinux:8
    PREDEPS_BUILD_ARG_SPACKDEPS: $PREDEPS_IMAGE_SPACK_ALMALINUX8_AMD64
'predeps: [ubuntu20.04-amd64]':
  needs: ['predeps: [spack-ubuntu20.04-amd64]']
  variables:
    PREDEPS_BUILD_ARG_BASE: docker.io/ubuntu:20.04
    PREDEPS_BUILD_ARG_SPACKDEPS: $PREDEPS_IMAGE_SPACK_UBUNTU20_04_AMD64
'predeps: [ubuntu20.04-arm64]':
  needs: ['predeps: [spack-ubuntu20.04-arm64]']
  variables:
    PREDEPS_BUILD_ARG_BASE: docker.io/ubuntu:20.04
    PREDEPS_BUILD_ARG_SPACKDEPS: $PREDEPS_IMAGE_SPACK_UBUNTU20_04_ARM64
'predeps: [fedora38-amd64]':
  needs: ['predeps: [spack-fedora37-amd64]']
  variables:
    PREDEPS_BUILD_ARG_BASE: docker.io/fedora:38
    PREDEPS_BUILD_ARG_SPACKDEPS: $PREDEPS_IMAGE_SPACK_FEDORA37_AMD64

'predeps: [min-almalinux8-amd64]':
  needs: ['predeps: [spack-min-almalinux8-amd64]']
  variables:
    PREDEPS_BUILD_ARG_BASE: quay.io/almalinux/almalinux:8
    PREDEPS_BUILD_ARG_SPACKDEPS: $PREDEPS_IMAGE_SPACK_MIN_ALMALINUX8_AMD64
'predeps: [min-ubuntu20.04-amd64]':
  needs: ['predeps: [spack-min-ubuntu20.04-amd64]']
  variables:
    PREDEPS_BUILD_ARG_BASE: docker.io/ubuntu:20.04
    PREDEPS_BUILD_ARG_SPACKDEPS: $PREDEPS_IMAGE_SPACK_MIN_UBUNTU20_04_AMD64
'predeps: [min-ubuntu20.04-arm64]':
  needs: ['predeps: [spack-min-ubuntu20.04-arm64]']
  variables:
    PREDEPS_BUILD_ARG_BASE: docker.io/ubuntu:20.04
    PREDEPS_BUILD_ARG_SPACKDEPS: $PREDEPS_IMAGE_SPACK_MIN_UBUNTU20_04_ARM64
'predeps: [min-fedora38-amd64]':
  needs: ['predeps: [spack-min-fedora37-amd64]']
  variables:
    PREDEPS_BUILD_ARG_BASE: docker.io/fedora:38
    PREDEPS_BUILD_ARG_SPACKDEPS: $PREDEPS_IMAGE_SPACK_MIN_FEDORA37_AMD64

'predeps: [cuda11.6-amd64]':
  needs: ['predeps: [spack-ubuntu20.04-amd64]']
  variables:
    PREDEPS_BUILD_ARG_BASE: docker.io/nvidia/cuda:11.6.2-devel-ubuntu20.04
    PREDEPS_BUILD_ARG_SPACKDEPS: $PREDEPS_IMAGE_SPACK_UBUNTU20_04_AMD64
'predeps: [cuda11.8-amd64]':
  needs: ['predeps: [spack-ubuntu20.04-amd64]']
  variables:
    PREDEPS_BUILD_ARG_BASE: docker.io/nvidia/cuda:11.8.0-devel-ubuntu20.04
    PREDEPS_BUILD_ARG_SPACKDEPS: $PREDEPS_IMAGE_SPACK_UBUNTU20_04_AMD64
'predeps: [cuda12.0-amd64]':
  needs: ['predeps: [spack-ubuntu20.04-amd64]']
  variables:
    PREDEPS_BUILD_ARG_BASE: docker.io/nvidia/cuda:12.0.1-devel-ubuntu20.04
    PREDEPS_BUILD_ARG_SPACKDEPS: $PREDEPS_IMAGE_SPACK_UBUNTU20_04_AMD64

'predeps: [min-cuda11.8-amd64]':
  needs: ['predeps: [spack-min-ubuntu20.04-amd64]']
  variables:
    PREDEPS_BUILD_ARG_BASE: docker.io/nvidia/cuda:11.8.0-devel-ubuntu20.04
    PREDEPS_BUILD_ARG_SPACKDEPS: $PREDEPS_IMAGE_SPACK_MIN_UBUNTU20_04_AMD64
'predeps: [min-cuda12.0-amd64]':
  needs: ['predeps: [spack-min-ubuntu20.04-amd64]']
  variables:
    PREDEPS_BUILD_ARG_BASE: docker.io/nvidia/cuda:12.0.1-devel-ubuntu20.04
    PREDEPS_BUILD_ARG_SPACKDEPS: $PREDEPS_IMAGE_SPACK_MIN_UBUNTU20_04_AMD64

'predeps: [rocm5.1-amd64]':
  needs: ['predeps: [spack-ubuntu20.04-amd64]']
  variables:
    PREDEPS_BUILD_ARG_BASE: docker.io/rocm/dev-ubuntu-20.04:5.1.3
    PREDEPS_BUILD_ARG_SPACKDEPS: $PREDEPS_IMAGE_SPACK_UBUNTU20_04_AMD64
'predeps: [rocm5.2-amd64]':
  needs: ['predeps: [spack-ubuntu20.04-amd64]']
  variables:
    PREDEPS_BUILD_ARG_BASE: docker.io/rocm/dev-ubuntu-20.04:5.2.3
    PREDEPS_BUILD_ARG_SPACKDEPS: $PREDEPS_IMAGE_SPACK_UBUNTU20_04_AMD64
'predeps: [rocm5.3-amd64]':
  needs: ['predeps: [spack-ubuntu20.04-amd64]']
  variables:
    PREDEPS_BUILD_ARG_BASE: docker.io/rocm/dev-ubuntu-20.04:5.3.2
    PREDEPS_BUILD_ARG_SPACKDEPS: $PREDEPS_IMAGE_SPACK_UBUNTU20_04_AMD64
'predeps: [rocm5.4-amd64]':
  needs: ['predeps: [spack-ubuntu20.04-amd64]']
  variables:
    PREDEPS_BUILD_ARG_BASE: docker.io/rocm/dev-ubuntu-20.04:5.4.2
    PREDEPS_BUILD_ARG_SPACKDEPS: $PREDEPS_IMAGE_SPACK_UBUNTU20_04_AMD64
'predeps: [rocm5.5-amd64]':
  needs: ['predeps: [spack-ubuntu20.04-amd64]']
  variables:
    PREDEPS_BUILD_ARG_BASE: docker.io/rocm/dev-ubuntu-20.04:5.5.1
    PREDEPS_BUILD_ARG_SPACKDEPS: $PREDEPS_IMAGE_SPACK_UBUNTU20_04_AMD64
'predeps: [rocm5.6-amd64]':
  needs: ['predeps: [spack-ubuntu20.04-amd64]']
  variables:
    PREDEPS_BUILD_ARG_BASE: docker.io/rocm/dev-ubuntu-20.04:5.6
    PREDEPS_BUILD_ARG_SPACKDEPS: $PREDEPS_IMAGE_SPACK_UBUNTU20_04_AMD64

'predeps: [min-rocm5.1-amd64]':
  needs: ['predeps: [spack-min-ubuntu20.04-amd64]']
  variables:
    PREDEPS_BUILD_ARG_BASE: docker.io/rocm/dev-ubuntu-20.04:5.1.3
    PREDEPS_BUILD_ARG_SPACKDEPS: $PREDEPS_IMAGE_SPACK_MIN_UBUNTU20_04_AMD64
'predeps: [min-rocm5.6-amd64]':
  needs: ['predeps: [spack-min-ubuntu20.04-amd64]']
  variables:
    PREDEPS_BUILD_ARG_BASE: docker.io/rocm/dev-ubuntu-20.04:5.6
    PREDEPS_BUILD_ARG_SPACKDEPS: $PREDEPS_IMAGE_SPACK_MIN_UBUNTU20_04_AMD64

'predeps: [intel-amd64]':
  needs: ['predeps: [spack-ubuntu20.04-amd64]']
  variables:
    PREDEPS_BUILD_ARG_BASE: docker.io/intel/oneapi-runtime:devel-ubuntu20.04
    PREDEPS_BUILD_ARG_SPACKDEPS: $PREDEPS_IMAGE_SPACK_UBUNTU20_04_AMD64
'predeps: [min-intel-amd64]':
  needs: ['predeps: [spack-min-ubuntu20.04-amd64]']
  variables:
    PREDEPS_BUILD_ARG_BASE: docker.io/intel/oneapi-runtime:devel-ubuntu20.04
    PREDEPS_BUILD_ARG_SPACKDEPS: $PREDEPS_IMAGE_SPACK_MIN_UBUNTU20_04_AMD64

#-----------------------------------
# Phase N: lint
#-----------------------------------

# Pre-commit linting passes must pass for the pipeline to succeed
precommit:
  stage: lint
  image: docker.io/python:3.10-bullseye
  needs: []
  before_script:
  - apt-get update -yqq && apt-get install -yqq git git-lfs
  - python3 -m pip install pre-commit
  script:
  - pre-commit run --all-files --show-diff-on-failure


# Check that the Spack recipe for @develop is sufficiently up-to-date to compile this branch.
# Failures are allowed for MRs so that if the dependencies or build flags shift, we are made aware
# (by a big orange exclaimation point) but not blocked by upstream Spack.
'spack install: [amd64]':
  stage: lint
  image: $PREDEPS_IMAGE_UBUNTU20_04_AMD64
  needs:
  - job: 'predeps: [ubuntu20.04-amd64]'
  - job: validate
    artifacts: false
  tags: [saas-linux-small-amd64]
  rules:
  - if: $CI_COMMIT_REF_PROTECTED == "true"
    allow_failure: false
  - allow_failure: true
  cache:
    key: spack
    when: always
    paths: [.spack.git]
  parallel:
    matrix:
    - VARIANTS:
      # TODO: Expand this list a bit sometime to better represent what we want to ensure works
      - +papi
      - ~papi
  before_script:
  - apt-get update -yqq
  - apt-get install -yqq git git-lfs gcc g++ make patch tar gzip unzip bzip2 xz-utils zstd file gnupg2 eatmydata
  # Ensure we have Git and Git LFS
  - git --version
  - git lfs --version
  script:
  # Instantiate a fresh Spack develop (instead of whatever is burned into the image)
  - |
    if [ ! -d .spack.git ]; then
      git clone --depth=30 --single-branch --no-tags --branch=develop --bare \
        https://github.com/spack/spack.git .spack.git || exit $?
    fi
  - git -C .spack.git fetch --verbose origin +develop:develop
  - git clone --shared --branch=develop .spack.git /tmp/spack
  - export PATH=/tmp/spack/bin:"$PATH"
  # Use a (temporary) Spack environment so that Spack will build from the current sources
  - spack env create -d /tmp/env
  - spack -D /tmp/env config add config:install_tree:root:/opt/software/
  - spack -D /tmp/env develop --no-clone --path "$CI_PROJECT_DIR" hpctoolkit@develop-ci
  - spack -D /tmp/env add "hpctoolkit @=develop-ci ~viewer ~mpi ~debug +opencl ~cuda ~rocm ~level_zero $VARIANTS"
  # Spack out a representative HPCToolkit. All dependencies should already be built.
  - eatmydata spack -D /tmp/env install --fail-fast --only=package hpctoolkit

#-----------------------------------
# Phase 2: validate
#-----------------------------------

# Ensure the autogoo is still up-to-date
validate:
  stage: validate
  image: $PREDEPS_IMAGE_UBUNTU20_04_AMD64
  tags: [saas-linux-small-amd64]
  script:
  - meson setup --native-file image.ini /tmp/builddir
  # Detect any changes to the main source directory
  - git status --porcelain=v1 --untracked-files=no > changes || exit $?
  - |
    if test -s changes; then
      echo "== CHANGES DETECTED, running git diff..."
      git diff | tee fixup.patch
      echo "== AUTOGOO OUT-OF-SYNC, see patch above and in fixup.patch for details"
      exit 1
    fi
  artifacts:
    when: always
    paths:
    - fixup.patch

#-----------------------------------
# Phase 3: test
#-----------------------------------

# Build many versions of the codebase, to ensure all the various compilations work
# As a general rule, we sweep the most-likely-to-fail configurations by turning off a single variant
# at a time. With the exception of +debug, which is flipped at will.
.buildmany:
  stage: test
  variables:
    JOB_CC: gcc
  script:
  - meson setup --native-file image.ini --native-file "$JOB_CC".ini --prefix /tmp/installdir /tmp/builddir
  - mkdir -p logs/"$CI_JOB_NAME"
  - |
    build() {
      id="$1"
      logfile=logs/"$CI_JOB_NAME"/"$id".log
      shift

      echo -e "\e[32m$ meson configure $* /tmp/builddir\e[0m"
      meson configure "$@" /tmp/builddir || return $?
      echo -e "\e[32m$ meson compile -C /tmp/builddir && meson install -C /tmp/builddir\e[0m"
      { meson compile -C /tmp/builddir && meson install -C /tmp/builddir; } > "$logfile" 2>&1 \
        || { code=$?; cat "$logfile"; return $code; }
    }
  - |
    for buildtype in debugoptimized release; do
      build $buildtype-all    -Dbuildtype=$buildtype -Dpapi=enabled -Dpython=enabled -Dopencl=enabled -Dhpcprof_mpi=enabled -Dvalgrind_annotations=true $COMMON_SETTINGS || exit $?
      build $buildtype-nopapi -Dbuildtype=$buildtype -Dpapi=disabled -Dpython=enabled -Dopencl=enabled -Dhpcprof_mpi=enabled -Dvalgrind_annotations=true $COMMON_SETTINGS || exit $?
      build $buildtype-nopy   -Dbuildtype=$buildtype -Dpapi=enabled -Dpython=disabled -Dopencl=enabled -Dhpcprof_mpi=enabled -Dvalgrind_annotations=true $COMMON_SETTINGS || exit $?
      build $buildtype-noocl  -Dbuildtype=$buildtype -Dpapi=enabled -Dpython=enabled -Dopencl=disabled -Dhpcprof_mpi=enabled -Dvalgrind_annotations=true $COMMON_SETTINGS || exit $?
      build $buildtype-nompi  -Dbuildtype=$buildtype -Dpapi=enabled -Dpython=enabled -Dopencl=enabled -Dhpcprof_mpi=disabled -Dvalgrind_annotations=true $COMMON_SETTINGS || exit $?
      build $buildtype-noval  -Dbuildtype=$buildtype -Dpapi=enabled -Dpython=enabled -Dopencl=enabled -Dhpcprof_mpi=enabled -Dvalgrind_annotations=false $COMMON_SETTINGS || exit $?
    done
  after_script:
  - |
    echo "To reproduce:"
    echo "    cd path/to/hpctoolkit"
    echo "    podman run --rm -it -v ./:/hpctoolkit --workdir /hpctoolkit -e JOB_CC=$JOB_CC $CI_JOB_IMAGE"
  - ./ci/predeps/best-python.sh ci/cc-diagnostics.py cq.json logs/"$CI_JOB_NAME"/*.log
  artifacts:
    reports:
      codequality: cq.json
    paths:
    - logs/
    when: always

# CPU-only spins
.buildmany amd64 cpu:
  extends: .buildmany
  tags: [saas-linux-small-amd64]
  variables:
    COMMON_SETTINGS: '-Dcuda=disabled -Drocm=disabled -Dlevel0=disabled'

'buildmany: [amd64, almalinux8]':
  extends: .buildmany amd64 cpu
  image: $PREDEPS_IMAGE_ALMALINUX8_AMD64
  parallel: {matrix: [{JOB_CC: gcc}]}
'buildmany: [amd64, ubuntu20.04]':
  extends: .buildmany amd64 cpu
  image: $PREDEPS_IMAGE_UBUNTU20_04_AMD64
  parallel: {matrix: [{JOB_CC: [clang10]}]}
'buildmany: [amd64, fedora38]':
  extends: .buildmany amd64 cpu
  image: $PREDEPS_IMAGE_FEDORA38_AMD64
  parallel: {matrix: [{JOB_CC: [gcc, clang]}]}

.buildmany min:
  variables:
    # Submodules should not be necessary for correct configuration
    GIT_SUBMODULE_STRATEGY: none

'buildmany min: [amd64, almalinux8]':
  extends: [.buildmany amd64 cpu, .buildmany min]
  image: $PREDEPS_IMAGE_MIN_ALMALINUX8_AMD64
  parallel: {matrix: [{JOB_CC: gcc}]}
'buildmany min: [amd64, ubuntu20.04]':
  extends: [.buildmany amd64 cpu, .buildmany min]
  image: $PREDEPS_IMAGE_MIN_UBUNTU20_04_AMD64
  parallel: {matrix: [{JOB_CC: [clang10]}]}
'buildmany min: [amd64, fedora38]':
  extends: [.buildmany amd64 cpu, .buildmany min]
  image: $PREDEPS_IMAGE_MIN_FEDORA38_AMD64
  parallel: {matrix: [{JOB_CC: [gcc, clang]}]}


# CUDA spins
.buildmany amd64 cuda:
  extends: .buildmany
  tags: [saas-linux-small-amd64]
  variables:
    COMMON_SETTINGS: '-Dcuda=enabled -Drocm=disabled -Dlevel0=disabled'

'buildmany: [amd64, cuda12.0]':
  extends: .buildmany amd64 cuda
  image: $PREDEPS_IMAGE_CUDA12_0_AMD64
'buildmany: [amd64, cuda11.8]':
  extends: .buildmany amd64 cuda
  image: $PREDEPS_IMAGE_CUDA11_8_AMD64

'buildmany min: [amd64, cuda12.0]':
  extends: [.buildmany amd64 cuda, .buildmany min]
  image: $PREDEPS_IMAGE_MIN_CUDA12_0_AMD64
'buildmany min: [amd64, cuda11.8]':
  extends: [.buildmany amd64 cuda, .buildmany min]
  image: $PREDEPS_IMAGE_MIN_CUDA11_8_AMD64

# ROCm spins
.buildmany amd64 rocm:
  extends: .buildmany
  tags: [saas-linux-small-amd64]
  variables:
    COMMON_SETTINGS: '-Dcuda=disabled -Drocm=enabled -Dlevel0=disabled'

'buildmany: [amd64, rocm5.1]':
  extends: .buildmany amd64 rocm
  image: $PREDEPS_IMAGE_ROCM5_1_AMD64
'buildmany: [amd64, rocm5.6]':
  extends: .buildmany amd64 rocm
  image: $PREDEPS_IMAGE_ROCM5_6_AMD64

'buildmany min: [amd64, rocm5.1]':
  extends: [.buildmany amd64 rocm, .buildmany min]
  image: $PREDEPS_IMAGE_MIN_ROCM5_1_AMD64
'buildmany min: [amd64, rocm5.6]':
  extends: [.buildmany amd64 rocm, .buildmany min]
  image: $PREDEPS_IMAGE_MIN_ROCM5_6_AMD64

# Level 0 + GTPin spins
.buildmany amd64 lvlz:
  extends: .buildmany
  tags: [saas-linux-small-amd64]
  variables:
    COMMON_SETTINGS: '-Dcuda=disabled -Drocm=disabled -Dlevel0=enabled -Dgtpin=enabled'
  script:
  - !reference [.buildmany, script]
  - |
    for buildtype in debugoptimized release; do
      build $buildtype-nogtpin -Dbuildtype=$buildtype -Dpapi=enabled -Dpython=enabled -Dopencl=enabled -Dhpcprof_mpi=enabled -Dvalgrind_annotations=true $COMMON_SETTINGS -Dgtpin=disabled || exit $?
    done

'buildmany: [amd64, lvlz2023.0, gtpin3.2]':
  extends: .buildmany amd64 lvlz
  image: $PREDEPS_IMAGE_INTEL_AMD64

'buildmany min: [amd64, lvlz2023.0, gtpin3.2]':
  extends: [.buildmany amd64 lvlz, .buildmany min]
  image: $PREDEPS_IMAGE_MIN_INTEL_AMD64


# Build single versions of the codebase and run unit tests through `make check`
# NB: We use --repeat in the check jobs to detect bugs that cause sporadic test failures.
# We chose --repeat 7, since this statistically provides:
#   - 99.2% confidence that this MR does not introduce a "blocking" bug that would prevent others'
#     work. We are very confident that a run will succeed >1/2 the time, and so require minimal
#     repeated commands to "push past" the issue. (`1 - pbinom(0, 7, 1 - 1/2) -> 0.992`)
#   - 79.0% confidence that this MR does not introduce a bug that would "annoy" others during their
#     work. We are modestly confident that a run will succeed >4/5 of the time, in which case the
#     bug may not be especially noticable. (`1 - pbinom(0, 7, 1 - 4/5) -> 0.790`)
#
# For the curious, the key formula to solve for the --repeat value is:
#     {# of repeats} > log(1 - {confidence}) / log({min prob of success})
# So a 99% confidence of 90% success rate requires a --repeat of at least 44.
.check:
  stage: test
  variables:
    # OpenMPI refuses to run as user 0 without these options set.
    OMPI_ALLOW_RUN_AS_ROOT: 1
    OMPI_ALLOW_RUN_AS_ROOT_CONFIRM: 1
  script:
  - &check_setup
    - mkdir logs/
    - logfile=logs/"$CI_JOB_NAME".log
    - rm -f "$logfile"
    - meson setup --native-file image.ini --prefix /tmp/installdir /tmp/builddir
  - &check_compile
    - meson compile -C /tmp/builddir >> "$logfile" 2>&1 || { code=$?; cat "$logfile"; exit $code; }
    - meson install -C /tmp/builddir >> "$logfile" 2>&1 || { code=$?; cat "$logfile"; exit $code; }
  - &check_test
    - meson test -C /tmp/builddir --maxfail=10 --repeat=${JOB_TEST_REPEAT:-7} ${JOB_TEST_SUITE:+--suite=$JOB_TEST_SUITE}
  after_script:
  - |
    echo "To reproduce:"
    echo "    cd path/to/hpctoolkit"
    echo "    podman run --rm -it -v ./:/hpctoolkit --workdir /hpctoolkit $CI_JOB_IMAGE"
  - &check_after
    - ./ci/predeps/best-python.sh ci/cc-diagnostics.py cq.json logs/"$CI_JOB_NAME".log
    - cp -t "$CI_PROJECT_DIR"/ /tmp/builddir/meson-logs/testlog.junit.xml

  artifacts: &check_artifacts
    reports:
      codequality: cq.json
      junit: '*.junit.xml'
    paths:
    - logs/
    when: always
.check semifresh job:
  script:
  - *check_setup
  - *check_compile
  - meson compile -C /tmp/builddir $(printf 'tests2/data/fresh-testdata-%s.tar.xz\n' $REGEN_SUITES)
  - mkdir -p testdata/"$CI_JOB_NAME"
  - cp -t testdata/"$CI_JOB_NAME"/ $(printf '/tmp/builddir/tests2/data/fresh-testdata-%s.tar.xz\n' $REGEN_SUITES)
  - for suite in $REGEN_SUITES; do tar xJvf /tmp/builddir/tests2/data/fresh-testdata-$suite.tar.xz || exit $?; done
  - *check_compile
  - *check_test
  after_script:
  - |
    echo "To reproduce:"
    echo "    cd path/to/hpctoolkit"
    echo "    podman run --rm -it -v ./:/hpctoolkit --workdir /hpctoolkit -e REGEN_SUITES='$REGEN_SUITES' $CI_JOB_IMAGE"
  - *check_after
  artifacts:
    <<: *check_artifacts
    paths:
    - logs/
    - testdata/

'check amd64: [cpu]':
  extends: .check
  image: $PREDEPS_IMAGE_UBUNTU20_04_AMD64
  tags: [rice-linux-large-bare-amd64]
  variables: &vars_check_cpu
    SPEC: '+mpi +debug +valgrind_debug +papi +python ~opencl ~cuda ~rocm ~level0'
'check min amd64: [cpu]':
  extends: .check
  image: $PREDEPS_IMAGE_MIN_UBUNTU20_04_AMD64
  tags: [rice-linux-large-bare-amd64]
  variables: *vars_check_cpu
'check semifresh amd64: [cpu]':
  extends: ['check amd64: [cpu]', .check semifresh job]
  variables:
    REGEN_SUITES: none cpu

'check arm64: [cpu]':
  extends: .check
  image: $PREDEPS_IMAGE_UBUNTU20_04_ARM64
  tags: [rice-linux-small-bare-arm64]
  variables:
    <<: *vars_check_cpu
    JOB_TEST_REPEAT: 1
'check min arm64: [cpu]':
  extends: .check
  image: $PREDEPS_IMAGE_MIN_UBUNTU20_04_ARM64
  tags: [rice-linux-small-bare-arm64]
  variables:
    <<: *vars_check_cpu
    JOB_TEST_REPEAT: 1

'check amd64: [+cuda, 11.6]':
  extends: .check
  image: $PREDEPS_IMAGE_CUDA11_6_AMD64
  tags: [rice-linux-large-amd64-gpu-p100]
  variables: &vars_check_cuda
    JOB_TEST_SUITE: cuda

'check amd64: [+cuda, 11.8]':
  extends: .check
  image: $PREDEPS_IMAGE_CUDA11_8_AMD64
  tags: [rice-linux-large-amd64-gpu-p100]
  variables: *vars_check_cuda
'check min amd64: [+cuda, 11.8]':
  extends: .check
  image: $PREDEPS_IMAGE_MIN_CUDA11_8_AMD64
  tags: [rice-linux-large-amd64-gpu-p100]
  variables: *vars_check_cuda
'check semifresh amd64: [+cuda, 11.8]':
  extends: ['check amd64: [+cuda, 11.8]', .check semifresh job]
  tags: [rice-linux-large-bare-amd64-gpu-p100]
  variables:
    REGEN_SUITES: none cpu nvidia sw-cuda

'check amd64: [+rocm, 5.1]':
  extends: .check
  image: $PREDEPS_IMAGE_ROCM5_1_AMD64
  tags: [rice-linux-large-amd64-gpu-amd]
  before_script: &bscript_check_rocm
  - export LD_LIBRARY_PATH=/opt/rocm/lib:"$LD_LIBRARY_PATH"
  variables: &vars_check_rocm
    JOB_TEST_SUITE: rocm

'check amd64: [+rocm, 5.2]':
  extends: .check
  image: $PREDEPS_IMAGE_ROCM5_2_AMD64
  tags: [rice-linux-large-amd64-gpu-amd]
  before_script: *bscript_check_rocm
  variables: *vars_check_rocm

'check amd64: [+rocm, 5.3]':
  extends: .check
  image: $PREDEPS_IMAGE_ROCM5_3_AMD64
  tags: [rice-linux-large-amd64-gpu-amd]
  before_script: *bscript_check_rocm
  variables: *vars_check_rocm

'check amd64: [+rocm, 5.4]':
  extends: .check
  image: $PREDEPS_IMAGE_ROCM5_4_AMD64
  tags: [rice-linux-large-amd64-gpu-amd]
  before_script: *bscript_check_rocm
  variables: *vars_check_rocm

'check amd64: [+rocm, 5.5]':
  extends: .check
  image: $PREDEPS_IMAGE_ROCM5_5_AMD64
  tags: [rice-linux-large-amd64-gpu-amd]
  before_script: *bscript_check_rocm
  variables: *vars_check_rocm

'check amd64: [+rocm, 5.6]':
  extends: .check
  image: $PREDEPS_IMAGE_ROCM5_6_AMD64
  tags: [rice-linux-large-amd64-gpu-amd]
  before_script: *bscript_check_rocm
  variables: *vars_check_rocm
'check min amd64: [+rocm, 5.6]':
  extends: 'check amd64: [+rocm, 5.6]'
  image: $PREDEPS_IMAGE_MIN_ROCM5_6_AMD64
'check semifresh amd64: [+rocm, 5.6]':
  extends: ['check amd64: [+rocm, 5.6]', .check semifresh job]
  tags: [rice-linux-large-bare-amd64-gpu-amd]
  variables:
    REGEN_SUITES: none cpu amd


# Repackage the fresh testdata generated during the `check semifresh` jobs
fresh testdata:
  stage: .post
  image: docker.io/alpine
  needs:
  - 'check semifresh amd64: [cpu]'
  - 'check semifresh amd64: [+cuda, 11.8]'
  - 'check semifresh amd64: [+rocm, 5.6]'
  when: always
  variables:
    GIT_SUBMODULE_STRATEGY: none
  script:
  - apk add xz
  - mkdir /tmp/result
  # Unpack all the tarballs to the temporary directory
  - "tar x -af testdata/'check semifresh amd64: [cpu]'/fresh-testdata-none.tar.xz -C /tmp/result"
  - "tar x -af testdata/'check semifresh amd64: [cpu]'/fresh-testdata-cpu.tar.xz -C /tmp/result"
  - "tar x -af testdata/'check semifresh amd64: [+cuda, 11.8]'/fresh-testdata-sw-cuda.tar.xz -C /tmp/result"
  - "tar x -af testdata/'check semifresh amd64: [+cuda, 11.8]'/fresh-testdata-nvidia.tar.xz -C /tmp/result"
  - "tar x -af testdata/'check semifresh amd64: [+rocm, 5.6]'/fresh-testdata-amd.tar.xz -C /tmp/result"
  # Repack into a single tarball
  - tar c -Jf testdata.tar.xz -C /tmp/result .
  artifacts:
    expose_as: "Regenerated test data"
    paths:
    - testdata.tar.xz
